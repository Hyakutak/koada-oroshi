# Oroshi

Oroshi est une **encyclopédie associative**.
Oroshi est une encyclopédie basée sur les associations d'entités. Imaginé sur la base des associations permaculturelles. Une association est un lien entre plusieurs entités permettant la création ou l'application de quelque chose. L'association comporte au minimum une ressource permettant l'application de cette dernière.

Exemple:
1) L'association d'une pelle et d'une rivière permet la création d'un étang. l'association comporte une vidéo / un livre / une description expliquant comment créer un étang.

#### Entités

Oroshi comporte différentes entités qui peuvent être associées:

- **Organisme**: Plante, micro-organisme, animal
- **Matériaux**:  Terre,  bois, fumier, acier, fruit.
- **Outil**: Perceuse, ordinateur, faux, led
- **Savoir**: Menuiserie, apiculture, mathématique
- **Environement**: Pente, forêt, pays, hiver

#### Associations

Oroshi comporte différentes associations qui peuvent relier x entités. Elle doit être une ressource permetant sa réalisation

- **Vidéo**
- **Livre**
- **Text**
- **Formation**

#### Status du projet

Status: **Définition du projet**
